/* Initializing dependencies */
const express = require('express');
const route = express.Router();

const util = require('../util');

module.exports = (settings, acc_conn, conn, fl, m, accm_conn, accfl_conn) =>
{
	/* Initializing general variables */
	// Default variable set for layout.pug
	const def = { title: 'Winter Fest' };
	
	/* Routing */
	route.get('/', async (req, res) =>
	{
		if(!req.hasOwnProperty('user')) // if not logged in
		{
			let variables = // variables for pug
				{
					error: req.flash('error'), // error message (used for error redirects)
					success: req.flash('success') // success message (used for success redirects)
				};
				
			if(util.isMobile(req.headers['user-agent'])) return res.render('mobile/root/login', Object.assign({}, def, variables));
			else return res.render('pc/root/login', Object.assign({}, def, variables)); // render login page if not logged in
		}
		
		if(req.user.accountLevel === 0) // if user is a student
		{
			let variables =
				{
					name: req.user.firstName, // firstname for welcome message
					username: req.user.username,
					displayname: req.user.firstName + ' ' + req.user.lastName,
					navBar: true, // show or hide navigation bar
					signups: [], // my passes list
					status: false, // if signup is opened or not
					activityname: settings.name,
					weekday: settings.weekday, // string weekday of the activity
					error: req.flash('error'),
					success: req.flash('success')
				};
			
			// if signup is opened set pug variable to true

			const params = await util.readParams(conn);
			if(params.enabled) variables.status = true;
			
			
			// add this week's passes
			try
			{
				if(req.user.gradYear == 2019||req.user.gradYear == 2022){
					const [rows] = (await fl.execute('SELECT * FROM `passes` WHERE `username` = ? AND `week` = ?', [req.user.username, params.week]));
					for(let c = 0; c < rows.length; c++) // for each rows
					{
						const row = rows[c];
						const teacher_uname = row.teacher; // teacher's username
						const [[teacherrow]] = await accfl_conn.execute('SELECT * FROM `teachers` WHERE `username` = ?', [teacher_uname]); // get teacher info with matching username
						await variables.signups.push({id: row.id, teacherName: `${teacherrow.firstName} ${teacherrow.lastName}`, room: teacherrow.room, purpose: row.purpose}); // push passes
					}
				}else{
					const [rows] = (await m.execute('SELECT * FROM `passes` WHERE `username` = ? AND `week` = ?', [req.user.username, params.week]));
					for(let c = 0; c < rows.length; c++) // for each rows
					{
						const row = rows[c];
						const teacher_uname = row.teacher; // teacher's username
						const [[teacherrow]] = await accm_conn.execute('SELECT * FROM `teachers` WHERE `username` = ?', [teacher_uname]); // get teacher info with matching username
						await variables.signups.push({id: row.id, teacherName: `${teacherrow.firstName} ${teacherrow.lastName}`, room: teacherrow.room, purpose: row.purpose}); // push passes
							
					}
				}
				
				if(util.isMobile(req.headers['user-agent'])) return res.render('mobile/root/index_student', Object.assign({}, def, variables));
				else return res.render('pc/root/index_student', Object.assign({}, def, variables));
			}
			catch(err)
			{
				req.flash('error', '500 Internal Server Error X(3');
				return res.redirect('/auth/logout'); // since all DB errors comes here by default, if an error continues to occur it would be reasonable to log them out
			}
		}
		if(req.user.accountLevel != 0){
			res.redirect('auth/logout');
		}
});
	route.get('/settings', (req, res) =>
	{

		if(!req.hasOwnProperty('user')) return res.redirect('/');

		if(req.user.accountLevel === 0)
		{	
			let variables = 
				{
					navBar: true,
					displayname: req.user.firstName + ' ' + req.user.lastName,
					name: req.user.firstName,
					google: req.user.googleID !== null,
					username: req.user.username,
					firstName: req.user.firstName,
					lastName: req.user.lastName,
					email: req.user.email,
					gradYear: req.user.gradYear,
					error: req.flash('error'),
					success: req.flash('success')
				};

			return res.render('pc/root/settings_student.pug', Object.assign({}, def, variables));
		}
		else if(req.user.accountLevel === 1)
		{
			let variables = 
				{
					navBar: true,
					displayname: req.user.firstName + ' ' + req.user.lastName,
					name: req.user.firstName,
					google: req.user.googleID !== null,
					username: req.user.username,
					firstName: req.user.firstName,
					lastName: req.user.lastName,
					email: req.user.email,
					userdepts: req.user.depts,
					depts: settings.depts,
					room: req.user.room,
					maxStudents: req.user.maxStudents,
					error: req.flash('error'),
					success: req.flash('success')
				};
			
			return res.render('pc/root/settings_teacher.pug', Object.assign({}, def, variables));
		}
		else return res.redirect('/');
	});

	route.post('/settings', async (req, res) =>
	{
		if(!req.hasOwnProperty('user')) return res.redirect('/');

		if(req.user.accountLevel === 0)
		{
			if(!(req.body.firstName && req.body.lastName && req.body.email && req.body.gradYear)) 
			{
				req.flash('error', 'Some fields are blank!');
				return res.redirect('/settings');
			}

			try
			{
				if(req.user.gradYear==2019||req.user.gradYear==2022){
				await acc_conn.execute('UPDATE `students` SET `firstName` = ?, `lastName` = ?, `email` = ?, `gradYear` = ? WHERE `username` = ?',
					[req.body.firstName, req.body.lastName, req.body.email, req.body.gradYear, req.user.username]);
				req.flash('success', 'Your personal settings have been successfully updated!');
				return res.redirect('/settings');
				}else{
				await acc_conn.execute('UPDATE `students` SET `firstName` = ?, `lastName` = ?, `email` = ?, `gradYear` = ? WHERE `username` = ?',
					[req.body.firstName, req.body.lastName, req.body.email, req.body.gradYear, req.user.username]);
				req.flash('success', 'Your personal settings have been successfully updated!');
				return res.redirect('/settings');
				}
			}
			catch(err)
			{
				req.flash('error', '500 Internal Server Error');
				return res.redirect('/');
			}
		}
		else if(req.user.accountLevel === 1)
		{
			if(!(req.body.firstName && req.body.lastName && req.body.email && req.body.depts && req.body.room && req.body.maxStudents))
			{
				req.flash('error', 'Some fields are blank!');
				return res.redirect('/settings');
			}

			let depts = req.body.depts;

			let departments = [];
			for(let c = 0; c < depts.length; c++)
			{
				if(!depts[c].startsWith('Department'))
				{
					let alreadyExists = false;
					for(let i = 0; i < departments.length; i++)
					{
						if(departments[i] === depts[c]) alreadyExists = true;
					}
					if(!alreadyExists) departments.push(depts[c]);
				}
			}

			try
			{
				if(req.user.gradYear==2019||req.user.gradYear==2022){
				await accfl_conn.execute('UPDATE `teachers` SET `firstName` = ?, `lastName` = ?, `email` = ?, `depts` = ?, `room` = ?, `maxStudents` = ? WHERE `username` = ?',
					[req.body.firstName, req.body.lastName, req.body.email, JSON.stringify(departments), req.body.room, req.body.maxStudents, req.user.username]);
				req.flash('success', 'Your personal settings have been successfully updated!<br><a href="/">Click Here to return to the front page.</a>');
				return res.redirect('/settings');
				}else{
					await accm_conn.execute('UPDATE `teachers` SET `firstName` = ?, `lastName` = ?, `email` = ?, `depts` = ?, `room` = ?, `maxStudents` = ? WHERE `username` = ?',
					[req.body.firstName, req.body.lastName, req.body.email, JSON.stringify(departments), req.body.room, req.body.maxStudents, req.user.username]);
				req.flash('success', 'Your personal settings have been successfully updated!<br><a href="/">Click Here to return to the front page.</a>');
				return res.redirect('/settings');
				}
			}
			catch(err)
			{
				req.flash('error', '500 Internal Server Error X(2');
				return res.redirect('/');
			}
		}
		else if(req.user.accountLevel === 2) res.redirect('/');

		else return res.redirect('/auth/logout');
	});
	
	route.get('/aboutus', (req, res) =>
	{
		// TODO: make aboutus page
		return res.send('Developed by David Kim');
	});
	
	return route;
};